//
//  ErrorManager.swift
//  MCDonald
//
//  Created by Alejandro Jiménez Agudo on 4/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation


class ErrorManager {
	
	class func errorMessage(error: NSError) -> String {
		guard let message = error.userInfo[Constants.ErrorMessage] as? String else {
			return kLocaleErrorUnexpected
		}
		
		return message
	}
	
}