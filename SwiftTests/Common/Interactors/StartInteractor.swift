//
//  StartInteractor.swift
//  MCDonald
//
//  Created by Alejandro Jiménez on 2/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation

enum StartResult {
	case Success
	case Error(String)
}


class StartInteractor {
	
	var configCommunicator = ConfigurationCommunicator()
	
	
	// MARK: - Public Methods
	
	func start(completionHandler: (StartResult) -> Void) {
		
		self.configCommunicator.fetchConfiguration { result in
			switch result {
			
			case .Success:
				completionHandler(.Success)
			
			case .Error(let error):
				completionHandler(.Error(ErrorManager.errorMessage(error)))
			}
		}
	}
	
}