//
//  NSLocale+GIGExtension.h
//  giglibrary
//
//  Created by Sergio Baró on 10/04/14.
//  Copyright (c) 2014 gigigo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSLocale (GIGExtension)

+ (NSString *)currentLanguage;

@end
