//
//  BaseWireframe.swift
//  MCDonald
//
//  Created by Alejandro Jiménez on 2/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//


import UIKit


class BaseWireframe {
	
	var window: UIWindow!
	
	func initialVC(storyboard: String) -> UIViewController? {
		let storyboard = UIStoryboard(name: storyboard, bundle: NSBundle.mainBundle())
		let vc = storyboard.instantiateInitialViewController()
		
		return vc
	}
	
	func viewController(storyboard: String, viewController: String) -> UIViewController? {
		let storyboard = UIStoryboard(name: storyboard, bundle: NSBundle.mainBundle())
		let vc = storyboard.instantiateViewControllerWithIdentifier(viewController)
		
		return vc
	}
	
	func presentModal(viewController: UIViewController) {
		guard let topVC = self.topViewController() else {
			LogWarn("Top view controller not found!")
			return
		}
		
		topVC.presentViewController(viewController, animated: true, completion: nil)
	}
	
	
	// MARK - Private Helpers
	
	private func topViewController() -> UIViewController? {
		var rootVC = UIApplication.sharedApplication().keyWindow?.rootViewController
		
		while let presentedController = rootVC?.presentedViewController {
			rootVC = presentedController
		}
		
		return rootVC
	}
	
}