/*
LocalizableConstants.swift

GENERATED - DO NOT MODIFY - use localio instead.

Created by localio
*/

import Foundation


let kLocaleAppName: String = { return NSLocalizedString("app_name", comment: "") }()
let kLocaleAlertButtonRetry: String = { return NSLocalizedString("alert_button_retry", comment: "") }()
let kLocaleErrorUnexpected: String = { return NSLocalizedString("error_unexpected", comment: "") }()
