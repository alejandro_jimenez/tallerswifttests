//
//  Constants.swift
//  MCDonald
//
//  Created by Alejandro Jiménez Agudo on 4/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation

class Constants {
	
	// ERRORS
	static let AppNetworkDomain  = "com.mcdonald.network"
	static let ErrorMessage		 = "MCDONALD_ERROR_MESSAGE"
	static let ErrorDebugMessage = "MCDONALD_ERROR_DEBUG_MESSAGE"
	
}