//
//  Response.swift
//  MCDonald
//
//  Created by Alejandro Jiménez Agudo on 4/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation


class Response {
	
	var success: Bool
	var statusCode: Int
	var json: [String: AnyObject]?
	var error: NSError?
	
	init() {
		self.success = false
		self.statusCode = -1
	}
	
}