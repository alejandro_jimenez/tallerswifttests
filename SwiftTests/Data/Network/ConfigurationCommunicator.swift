//
//  ConfigurationCommunicator.swift
//  MCDonald
//
//  Created by Alejandro Jiménez Agudo on 4/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation
import GIGLibrary


enum ConfigurationCommunicatorResult {
	case Success
	case Error(NSError)
}


class ConfigurationCommunicator {
	
	func fetchConfiguration(completionHandler: (ConfigurationCommunicatorResult) -> Void) {
		
		Request (
			endpoint: "/configuration",
			method: "GET"
		).sendAsync { response in
			if response.success {
				completionHandler(.Success)
			}
			else {
				let error = response.error ?? NSError(domain: Constants.AppNetworkDomain, code: -1, userInfo: nil)
				completionHandler(.Error(error))
			}
		}		
	}
	
}