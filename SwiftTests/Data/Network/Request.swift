//
//  Request.swift
//  MCDonald
//
//  Created by Alejandro Jiménez Agudo on 4/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation


class Request {
	
	var endpoint: String
	var method: String
	var headers: [String: String]
	var urlParams: [String: AnyObject]
	var bodyParams: [String: AnyObject]
	var verbose = false
	
	init(endpoint: String, method: String, headers: [String: String] = [:], urlParams: [String: AnyObject] = [:], bodyParams: [String: AnyObject] = [:]) {
		self.endpoint = endpoint
		self.method = method
		self.headers = headers
		self.urlParams = urlParams
		self.bodyParams = bodyParams
	}
	
	
	func sendAsync(completionHandler: (Response) -> Void) {
		completionHandler(Response())
	}
	
}