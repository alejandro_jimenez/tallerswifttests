//
//  AppDelegate.swift
//  MCDonald
//
//  Created by Alejandro Jiménez Agudo on 1/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	var appController = AppController()
	

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
		self.initializeExternalLibs()
		
		self.prepareAppController()
		self.window?.makeKeyAndVisible()
		
		self.appController.appDidLaunch()
		
		return true
	}
	
	
	// MARK - Private Helpers
	
	private func initializeExternalLibs() {
		LogManager.shared.appName = kLocaleAppName
		LogManager.shared.logLevel = .Debug
	}
	
	private func prepareAppController() {
		self.appController.appWireframe = AppWireframe()
		self.appController.appWireframe.window = self.window
		self.appController.appWireframe.appController = self.appController
	}

}

