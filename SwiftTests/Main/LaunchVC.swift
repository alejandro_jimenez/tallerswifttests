//
//  LaunchVC.swift
//  MCDonald
//
//  Created by Alejandro Jiménez on 2/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import UIKit

class LaunchVC: BaseViewController, LaunchView {
	
	var presenter: LaunchPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		self.presenter.viewDidLoad()
	}
	
	
	// MARK: - Presenter
	
	func showLoading() {
		UIApplication.sharedApplication().networkActivityIndicatorVisible = true
	}
	
	func hideLoading() {
		UIApplication.sharedApplication().networkActivityIndicatorVisible = false
	}
	
	func showRetryAlert(message: String) {
		let alert = UIAlertController(title: kLocaleAppName, message: message, preferredStyle: .Alert)
		
		let actionRetry = UIAlertAction(title: kLocaleAlertButtonRetry, style: .Default) { _ in
			self.presenter.userDidTapRetry()
		}

		alert.addAction(actionRetry)
		
		self.presentViewController(alert, animated: true, completion: nil)
	}

}
