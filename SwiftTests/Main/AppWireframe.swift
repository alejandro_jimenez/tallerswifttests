//
//  AppWireframe.swift
//  MCDonald
//
//  Created by Alejandro Jiménez on 2/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import UIKit
//import GIGLibrary


class AppWireframe: BaseWireframe {
	
	var appController: AppController!
	
	private var navigationController: UINavigationController!

	
	// MARK - Public Methods
	
	func showSplash() {
		guard let launchVC = self.initialVC() as? LaunchVC else {
			LogWarn("LaunchVC not found")
			return
		}
		
		launchVC.presenter = LaunchPresenter()
		launchVC.presenter.appController = self.appController
		launchVC.presenter.view = launchVC
	
		self.window.rootViewController = launchVC
	}
	
	func showWelcome() {
		guard let welcomeVC = self.viewController("WelcomeVC") else {
			LogWarn("WelcomeVC not found")
			return
		}

		self.navigationController = UINavigationController(rootViewController: welcomeVC)
		self.navigationController.modalTransitionStyle = .CrossDissolve
		self.presentModal(self.navigationController)
	}
	
	
	// MARK - Private Helpers
	
	private func initialVC() -> UIViewController? {
		return self.initialVC("Start")
	}
	
	private func viewController(viewController: String) -> UIViewController? {
		return self.viewController("Start", viewController: viewController)
	}
	
}