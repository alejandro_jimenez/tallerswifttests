//
//  AppController.swift
//  MCDonald
//
//  Created by Alejandro Jiménez on 2/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation


class AppController {
	
	var appWireframe: AppWireframe!
	
	func appDidLaunch() {
		LogInfo("Application did start!")
		self.appWireframe.showSplash()
	}
	
	func launchDidFinish() {
		self.appWireframe.showWelcome()
	}
	
}
