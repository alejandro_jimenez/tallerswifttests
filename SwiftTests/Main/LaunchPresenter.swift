//
//  LaunchPresenter.swift
//  MCDonald
//
//  Created by Alejandro Jiménez on 2/2/16.
//  Copyright © 2016 Gigigo SL. All rights reserved.
//

import Foundation


protocol LaunchView {
	func showLoading()
	func hideLoading()
	
	func showRetryAlert(message: String)
}


class LaunchPresenter {
	
	var view: LaunchView!
	var appController: AppController!

	private var startInteractor = StartInteractor()
		
	
	// MARK: - Public Methods
	
	func viewDidLoad() {
		self.start()
	}
	
	func userDidTapRetry() {
		self.start()
	}
	
	
	// MARK: - Private Helpers
	
	private func start() {
		self.view.showLoading()
		self.startInteractor.start { result in
			self.view.hideLoading()
			
			switch result {
				
			case .Success:
				self.view.hideLoading()
				self.appController.launchDidFinish()
				
			case .Error(let message):
				self.view.showRetryAlert(message)
			}
		}
	}
	
}
